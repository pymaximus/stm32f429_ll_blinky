low level example
=================

Simple STM32CubeMX (v4.26.1) Low Level (LL) Blinky example on STM32F429 Discovery Board.

Uses Makefile target from STM32CubeMX. SYSCLK = 180MHz.


GCC_PATH=/Users/frank/opt/gcc-arm-none-eabi-7-2018-q2-update/bin/ make


``` text
✔ ~/repos/stm32f429_ll_blinky [master L|…1] 
21:30 $ GCC_PATH=/Users/frank/opt/gcc-arm-none-eabi-7-2018-q2-update/bin/ make
mkdir build		
/Users/frank/opt/gcc-arm-none-eabi-7-2018-q2-update/bin//arm-none-eabi-gcc -c -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -DUSE_FULL_LL_DRIVER -DHSE_VALUE=8000000 -DHSE_STARTUP_TIMEOUT=100 -DLSE_STARTUP_TIMEOUT=5000 -DLSE_VALUE=32768 -DEXTERNAL_CLOCK_VALUE=12288000 -DHSI_VALUE=16000000 -DLSI_VALUE=32000 -DVDD_VALUE=3300 -DPREFETCH_ENABLE=1 -DINSTRUCTION_CACHE_ENABLE=1 -DDATA_CACHE_ENABLE=1 -DSTM32F429xx -IInc -IDrivers/STM32F4xx_HAL_Driver/Inc -IDrivers/CMSIS/Device/ST/STM32F4xx/Include -IDrivers/CMSIS/Include -O3 -Wall -fdata-sections -ffunction-sections -g -gdwarf-2 -MMD -MP -MF"build/main.d" -Wa,-a,-ad,-alms=build/main.lst Src/main.c -o build/main.o
/Users/frank/opt/gcc-arm-none-eabi-7-2018-q2-update/bin//arm-none-eabi-gcc -c -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -DUSE_FULL_LL_DRIVER -DHSE_VALUE=8000000 -DHSE_STARTUP_TIMEOUT=100 -DLSE_STARTUP_TIMEOUT=5000 -DLSE_VALUE=32768 -DEXTERNAL_CLOCK_VALUE=12288000 -DHSI_VALUE=16000000 -DLSI_VALUE=32000 -DVDD_VALUE=3300 -DPREFETCH_ENABLE=1 -DINSTRUCTION_CACHE_ENABLE=1 -DDATA_CACHE_ENABLE=1 -DSTM32F429xx -IInc -IDrivers/STM32F4xx_HAL_Driver/Inc -IDrivers/CMSIS/Device/ST/STM32F4xx/Include -IDrivers/CMSIS/Include -O3 -Wall -fdata-sections -ffunction-sections -g -gdwarf-2 -MMD -MP -MF"build/gpio.d" -Wa,-a,-ad,-alms=build/gpio.lst Src/gpio.c -o build/gpio.o
/Users/frank/opt/gcc-arm-none-eabi-7-2018-q2-update/bin//arm-none-eabi-gcc -c -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -DUSE_FULL_LL_DRIVER -DHSE_VALUE=8000000 -DHSE_STARTUP_TIMEOUT=100 -DLSE_STARTUP_TIMEOUT=5000 -DLSE_VALUE=32768 -DEXTERNAL_CLOCK_VALUE=12288000 -DHSI_VALUE=16000000 -DLSI_VALUE=32000 -DVDD_VALUE=3300 -DPREFETCH_ENABLE=1 -DINSTRUCTION_CACHE_ENABLE=1 -DDATA_CACHE_ENABLE=1 -DSTM32F429xx -IInc -IDrivers/STM32F4xx_HAL_Driver/Inc -IDrivers/CMSIS/Device/ST/STM32F4xx/Include -IDrivers/CMSIS/Include -O3 -Wall -fdata-sections -ffunction-sections -g -gdwarf-2 -MMD -MP -MF"build/stm32f4xx_it.d" -Wa,-a,-ad,-alms=build/stm32f4xx_it.lst Src/stm32f4xx_it.c -o build/stm32f4xx_it.o
/Users/frank/opt/gcc-arm-none-eabi-7-2018-q2-update/bin//arm-none-eabi-gcc -c -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -DUSE_FULL_LL_DRIVER -DHSE_VALUE=8000000 -DHSE_STARTUP_TIMEOUT=100 -DLSE_STARTUP_TIMEOUT=5000 -DLSE_VALUE=32768 -DEXTERNAL_CLOCK_VALUE=12288000 -DHSI_VALUE=16000000 -DLSI_VALUE=32000 -DVDD_VALUE=3300 -DPREFETCH_ENABLE=1 -DINSTRUCTION_CACHE_ENABLE=1 -DDATA_CACHE_ENABLE=1 -DSTM32F429xx -IInc -IDrivers/STM32F4xx_HAL_Driver/Inc -IDrivers/CMSIS/Device/ST/STM32F4xx/Include -IDrivers/CMSIS/Include -O3 -Wall -fdata-sections -ffunction-sections -g -gdwarf-2 -MMD -MP -MF"build/stm32f4xx_ll_gpio.d" -Wa,-a,-ad,-alms=build/stm32f4xx_ll_gpio.lst Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_gpio.c -o build/stm32f4xx_ll_gpio.o
/Users/frank/opt/gcc-arm-none-eabi-7-2018-q2-update/bin//arm-none-eabi-gcc -c -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -DUSE_FULL_LL_DRIVER -DHSE_VALUE=8000000 -DHSE_STARTUP_TIMEOUT=100 -DLSE_STARTUP_TIMEOUT=5000 -DLSE_VALUE=32768 -DEXTERNAL_CLOCK_VALUE=12288000 -DHSI_VALUE=16000000 -DLSI_VALUE=32000 -DVDD_VALUE=3300 -DPREFETCH_ENABLE=1 -DINSTRUCTION_CACHE_ENABLE=1 -DDATA_CACHE_ENABLE=1 -DSTM32F429xx -IInc -IDrivers/STM32F4xx_HAL_Driver/Inc -IDrivers/CMSIS/Device/ST/STM32F4xx/Include -IDrivers/CMSIS/Include -O3 -Wall -fdata-sections -ffunction-sections -g -gdwarf-2 -MMD -MP -MF"build/stm32f4xx_ll_exti.d" -Wa,-a,-ad,-alms=build/stm32f4xx_ll_exti.lst Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_exti.c -o build/stm32f4xx_ll_exti.o
/Users/frank/opt/gcc-arm-none-eabi-7-2018-q2-update/bin//arm-none-eabi-gcc -c -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -DUSE_FULL_LL_DRIVER -DHSE_VALUE=8000000 -DHSE_STARTUP_TIMEOUT=100 -DLSE_STARTUP_TIMEOUT=5000 -DLSE_VALUE=32768 -DEXTERNAL_CLOCK_VALUE=12288000 -DHSI_VALUE=16000000 -DLSI_VALUE=32000 -DVDD_VALUE=3300 -DPREFETCH_ENABLE=1 -DINSTRUCTION_CACHE_ENABLE=1 -DDATA_CACHE_ENABLE=1 -DSTM32F429xx -IInc -IDrivers/STM32F4xx_HAL_Driver/Inc -IDrivers/CMSIS/Device/ST/STM32F4xx/Include -IDrivers/CMSIS/Include -O3 -Wall -fdata-sections -ffunction-sections -g -gdwarf-2 -MMD -MP -MF"build/stm32f4xx_ll_rcc.d" -Wa,-a,-ad,-alms=build/stm32f4xx_ll_rcc.lst Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_rcc.c -o build/stm32f4xx_ll_rcc.o
/Users/frank/opt/gcc-arm-none-eabi-7-2018-q2-update/bin//arm-none-eabi-gcc -c -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -DUSE_FULL_LL_DRIVER -DHSE_VALUE=8000000 -DHSE_STARTUP_TIMEOUT=100 -DLSE_STARTUP_TIMEOUT=5000 -DLSE_VALUE=32768 -DEXTERNAL_CLOCK_VALUE=12288000 -DHSI_VALUE=16000000 -DLSI_VALUE=32000 -DVDD_VALUE=3300 -DPREFETCH_ENABLE=1 -DINSTRUCTION_CACHE_ENABLE=1 -DDATA_CACHE_ENABLE=1 -DSTM32F429xx -IInc -IDrivers/STM32F4xx_HAL_Driver/Inc -IDrivers/CMSIS/Device/ST/STM32F4xx/Include -IDrivers/CMSIS/Include -O3 -Wall -fdata-sections -ffunction-sections -g -gdwarf-2 -MMD -MP -MF"build/stm32f4xx_ll_utils.d" -Wa,-a,-ad,-alms=build/stm32f4xx_ll_utils.lst Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_utils.c -o build/stm32f4xx_ll_utils.o
/Users/frank/opt/gcc-arm-none-eabi-7-2018-q2-update/bin//arm-none-eabi-gcc -c -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -DUSE_FULL_LL_DRIVER -DHSE_VALUE=8000000 -DHSE_STARTUP_TIMEOUT=100 -DLSE_STARTUP_TIMEOUT=5000 -DLSE_VALUE=32768 -DEXTERNAL_CLOCK_VALUE=12288000 -DHSI_VALUE=16000000 -DLSI_VALUE=32000 -DVDD_VALUE=3300 -DPREFETCH_ENABLE=1 -DINSTRUCTION_CACHE_ENABLE=1 -DDATA_CACHE_ENABLE=1 -DSTM32F429xx -IInc -IDrivers/STM32F4xx_HAL_Driver/Inc -IDrivers/CMSIS/Device/ST/STM32F4xx/Include -IDrivers/CMSIS/Include -O3 -Wall -fdata-sections -ffunction-sections -g -gdwarf-2 -MMD -MP -MF"build/system_stm32f4xx.d" -Wa,-a,-ad,-alms=build/system_stm32f4xx.lst Src/system_stm32f4xx.c -o build/system_stm32f4xx.o
/Users/frank/opt/gcc-arm-none-eabi-7-2018-q2-update/bin//arm-none-eabi-gcc -x assembler-with-cpp -c -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -DUSE_FULL_LL_DRIVER -DHSE_VALUE=8000000 -DHSE_STARTUP_TIMEOUT=100 -DLSE_STARTUP_TIMEOUT=5000 -DLSE_VALUE=32768 -DEXTERNAL_CLOCK_VALUE=12288000 -DHSI_VALUE=16000000 -DLSI_VALUE=32000 -DVDD_VALUE=3300 -DPREFETCH_ENABLE=1 -DINSTRUCTION_CACHE_ENABLE=1 -DDATA_CACHE_ENABLE=1 -DSTM32F429xx -IInc -IDrivers/STM32F4xx_HAL_Driver/Inc -IDrivers/CMSIS/Device/ST/STM32F4xx/Include -IDrivers/CMSIS/Include -O3 -Wall -fdata-sections -ffunction-sections -g -gdwarf-2 -MMD -MP -MF"build/startup_stm32f429xx.d" startup_stm32f429xx.s -o build/startup_stm32f429xx.o
/Users/frank/opt/gcc-arm-none-eabi-7-2018-q2-update/bin//arm-none-eabi-gcc build/main.o build/gpio.o build/stm32f4xx_it.o build/stm32f4xx_ll_gpio.o build/stm32f4xx_ll_exti.o build/stm32f4xx_ll_rcc.o build/stm32f4xx_ll_utils.o build/system_stm32f4xx.o build/startup_stm32f429xx.o -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -specs=nano.specs -TSTM32F429ZITx_FLASH.ld  -lc -lm -lnosys  -Wl,-Map=build/stm32f429_ll_blinky.map,--cref -Wl,--gc-sections -o build/stm32f429_ll_blinky.elf
/Users/frank/opt/gcc-arm-none-eabi-7-2018-q2-update/bin//arm-none-eabi-size build/stm32f429_ll_blinky.elf
   text	   data	    bss	    dec	    hex	filename
   1640	     12	   1564	   3216	    c90	build/stm32f429_ll_blinky.elf
/Users/frank/opt/gcc-arm-none-eabi-7-2018-q2-update/bin//arm-none-eabi-objcopy -O ihex build/stm32f429_ll_blinky.elf build/stm32f429_ll_blinky.hex
/Users/frank/opt/gcc-arm-none-eabi-7-2018-q2-update/bin//arm-none-eabi-objcopy -O binary -S build/stm32f429_ll_blinky.elf build/stm32f429_ll_blinky.bin	
```

PG13 should be toggling at  high rate (~ 6.00MHz) if we toggle pin using:

``` c
  while (1)
  {
      LL_GPIO_TogglePin(GPIOG, LL_GPIO_PIN_13);
  }
```



You can get faster (~36 MHz ) using:


``` c
  while (1)
  {
      GPIOG->BSRR = GPIO_BSRR_BR13;
      GPIOG->BSRR = GPIO_BSRR_BS13;
  }
```

Still, there is delay traversing the loop (see Assembler output shown later).
Checking with oscilloscope, PG13 output pin on discovery board.

![Screenshot](images/single_toggle.png)

The delay is about 18nSec, due to extra instructions.

![Screenshot](images/loop_delay.png)


And if you unroll the loop, you get 90MHz which seems max on AHB1
F = 90MHz, means T=11.11nSec

``` c
      // Burst of 6 cycles
      LL_GPIO_SetOutputPin(GPIOG, LL_GPIO_PIN_13);
      LL_GPIO_ResetOutputPin(GPIOG, LL_GPIO_PIN_13);
      
      LL_GPIO_SetOutputPin(GPIOG, LL_GPIO_PIN_13);
      LL_GPIO_ResetOutputPin(GPIOG, LL_GPIO_PIN_13);
      
      LL_GPIO_SetOutputPin(GPIOG, LL_GPIO_PIN_13);
      LL_GPIO_ResetOutputPin(GPIOG, LL_GPIO_PIN_13);
      
      LL_GPIO_SetOutputPin(GPIOG, LL_GPIO_PIN_13);
      LL_GPIO_ResetOutputPin(GPIOG, LL_GPIO_PIN_13);
      
      LL_GPIO_SetOutputPin(GPIOG, LL_GPIO_PIN_13);
      LL_GPIO_ResetOutputPin(GPIOG, LL_GPIO_PIN_13);

      LL_GPIO_SetOutputPin(GPIOG, LL_GPIO_PIN_13);
      LL_GPIO_ResetOutputPin(GPIOG, LL_GPIO_PIN_13);
      
```

![Screenshot](images/burst.png)

If you look at the assembler, is is basically this

``` assembly
 464 0072 9961     		str	r1, [r3, #24]
 471 0074 9A61     		str	r2, [r3, #24]

```



Awesome !!



main.lst layout
---------------

Loop runs from 006c (.L18) to 007c when we use GPIO->BSRR toggle in loop.

``` text
110:Src/main.c    ****       GPIOG->BSRR = GPIO_BSRR_BS13;
 538              		.loc 1 110 0
 539 0068 4FF00050 		mov	r0, #536870912
 111:Src/main.c    ****       
 540              		.loc 1 111 0
 541 006c 4FF40051 		mov	r1, #8192
 542              	.L18:
 543              	.LVL39:
 544              	.LBB332:
 545              	.LBB331:
 546              		.loc 8 958 0 discriminator 1
 547 0070 5A69     		ldr	r2, [r3, #20]
 548 0072 82F40052 		eor	r2, r2, #8192
 549 0076 5A61     		str	r2, [r3, #20]
 550              	.LVL40:
 551              	.LBE331:
 552              	.LBE332:
 110:Src/main.c    ****       GPIOG->BSRR = GPIO_BSRR_BS13;
 553              		.loc 1 110 0 discriminator 1
 554 0078 9861     		str	r0, [r3, #24]
 111:Src/main.c    ****       
 555              		.loc 1 111 0 discriminator 1
 556 007a 9961     		str	r1, [r3, #24]
 557 007c F8E7     		b	.L18
 558              	.L21:
```

Unrolled Loop, 6 bursts
-----------------------


``` c
455              		.loc 8 929 0
 456 006e 4FF00052 		mov	r2, #536870912
 457              	.L15:
 458              	.LVL41:
 459              	.LBE301:
 460              	.LBE300:
 461              	.LBB303:
 462              	.LBB299:
 900:Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_ll_gpio.h **** }
 463              		.loc 8 900 0 discriminator 1
 464 0072 9961     		str	r1, [r3, #24]
 465              	.LVL42:
 466              	.LBE299:
 467              	.LBE303:
 468              	.LBB304:
 469              	.LBB302:
 470              		.loc 8 929 0 discriminator 1
 471 0074 9A61     		str	r2, [r3, #24]
 472              	.LVL43:
 473              	.LBE302:
 474              	.LBE304:
 475              	.LBB305:
 476              	.LBB306:
 900:Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_ll_gpio.h **** }
 477              		.loc 8 900 0 discriminator 1
 478 0076 9961     		str	r1, [r3, #24]
 479              	.LVL44:
 480              	.LBE306:
 481              	.LBE305:
 482              	.LBB307:
ARM GAS  /var/folders/ly/kpxp6_bj62q3vm96hl6t_jzr0000gn/T//ccIJfGzm.s 			page 208


 483              	.LBB308:
 484              		.loc 8 929 0 discriminator 1
 485 0078 9A61     		str	r2, [r3, #24]
 486              	.LVL45:
 487              	.LBE308:
 488              	.LBE307:
 489              	.LBB309:
 490              	.LBB310:
 900:Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_ll_gpio.h **** }
 491              		.loc 8 900 0 discriminator 1
 492 007a 9961     		str	r1, [r3, #24]
 493              	.LVL46:
 494              	.LBE310:
 495              	.LBE309:
 496              	.LBB311:
 497              	.LBB312:
 498              		.loc 8 929 0 discriminator 1
 499 007c 9A61     		str	r2, [r3, #24]
 500              	.LVL47:
 501              	.LBE312:
 502              	.LBE311:
 503              	.LBB313:
 504              	.LBB314:
 900:Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_ll_gpio.h **** }
 505              		.loc 8 900 0 discriminator 1
 506 007e 9961     		str	r1, [r3, #24]
 507              	.LVL48:
 508              	.LBE314:
 509              	.LBE313:
 510              	.LBB315:
 511              	.LBB316:
 512              		.loc 8 929 0 discriminator 1
 513 0080 9A61     		str	r2, [r3, #24]
 514              	.LVL49:
 515              	.LBE316:
 516              	.LBE315:
 517              	.LBB317:
 518              	.LBB318:
 900:Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_ll_gpio.h **** }
 519              		.loc 8 900 0 discriminator 1
 520 0082 9961     		str	r1, [r3, #24]
 521              	.LVL50:
 522              	.LBE318:
 523              	.LBE317:
 524              	.LBB319:
 525              	.LBB320:
 526              		.loc 8 929 0 discriminator 1
 527 0084 9A61     		str	r2, [r3, #24]
 528              	.LVL51:
 529              	.LBE320:
 530              	.LBE319:
 531              	.LBB321:
 532              	.LBB322:
 900:Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_ll_gpio.h **** }
 533              		.loc 8 900 0 discriminator 1
 534 0086 9961     		str	r1, [r3, #24]
 535              	.LVL52:
ARM GAS  /var/folders/ly/kpxp6_bj62q3vm96hl6t_jzr0000gn/T//ccIJfGzm.s 			page 209


 536              	.LBE322:
 537              	.LBE321:
 538              	.LBB323:
 539              	.LBB324:
 540              		.loc 8 929 0 discriminator 1
 541 0088 9A61     		str	r2, [r3, #24]
 542 008a F2E7     		b	.L15
 543              	.L18:
 544              		.align	2
 545              	.L17:
```




Burst Mode with MCO1 Div2 (90MHz)
---------------------------------

This example shows MCO1 DIV2 at 90MHz (SYSCLK=180MHz) in top (YELLOW) trace.

The bottom BLUE trace is PG13 output showing 6 cycles in a burst every pass through
the while(1) loop.

![Screenshot](images/burst_with_mco1_div2.png)


So, we can get 90MHz output on PG13 via loop unrolling !!

Awesome !!

Note: If you are concerned with the overshoot of the trace, its because of the long ground clip connection
from probe to board. It would look better if there was a GND point right next to PG13  :-)



Unusual behaviour after pushing RESET button on STM32F4 Discovery board (Solved)
--------------------------------------------------------------------------------


There was an outstanding question related to the change in output when **RESET** button is 
pushed on the board. The pulse stream moves from 6 consecutive pulses to 4 pulses + delay + 2 pulses.

See the following screen capture for PG13 output after **RESET** button pushed.


![Screenshot](images/funky_pulse_stream.png)


Well it appears that we can improve matters by enabling Caching and Prefetch.

It was specified in Makefile generated by Cube, but that alone does not allow for FLASH_ACR being reset back to 0
at reset. 

From en.DM00031020.pdf (RM0090)

``` text
3.9.1 Flash access control register (FLASH_ACR) for STM32F405xx/07xx and STM32F415xx/17xx

The Flash access control register is used to enable/disable the acceleration 
features and control the Flash memory access time according to CPU frequency.

Address offset: 0x00 

Reset value: 0x0000 0000
```

Cube generated Makefile

``` c
# C defines
C_DEFS =  \
-DUSE_FULL_LL_DRIVER \
-DHSE_VALUE=8000000 \
-DHSE_STARTUP_TIMEOUT=100 \
-DLSE_STARTUP_TIMEOUT=5000 \
-DLSE_VALUE=32768 \
-DEXTERNAL_CLOCK_VALUE=12288000 \
-DHSI_VALUE=16000000 \
-DLSI_VALUE=32000 \
-DVDD_VALUE=3300 \
-DPREFETCH_ENABLE=1 \
-DINSTRUCTION_CACHE_ENABLE=1 \
-DDATA_CACHE_ENABLE=1 \
-DSTM32F429xx

```

So, the following code was added ..

``` c
  /* Enable Prefetch and caching, otherwise hitting reset button on STM32F429 Discovery */
  /* board will cause FLASH_ACR to be reset. NOTE: Reset_Handler also does not set these. */
  
  LL_FLASH_EnableInstCache();
  LL_FLASH_EnablePrefetch();
  LL_FLASH_EnableDataCache();

```


Now it does not matter how many times I hit the reset button on the board, the 6 cycle burst is no longer
interrupted as before.

Here is the updated performance.

![Screenshot](images/enable_cache_and_prefetch.png)

Awesome !!


